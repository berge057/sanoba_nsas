
#data source :
#https://psl.noaa.gov/cgi-bin/db_search/DBSearch.pl?Dataset=ICOADS+2-degree+Standard&Dataset=ICOADS+2-degree+Enhanced&Variable=Sea+Surface+Temperature
#
library(ncdf4)
library(ncdf4.helpers)
library(ggplot2)
library(FLCore)

rm(list=ls())

#path <- "J:/git/sanoba_nsas"
path <- "C:/git/sanoba_nsas"

try(setwd(path),silent=TRUE)

################################################
# setup paths
################################################

data.dir        <- file.path(".","data")
script.dir      <-  file.path(".","side_scripts/")
function.dir    <-  file.path(".","functions/")
results.dir     <-  file.path(".","results/")
model.dir       <-  file.path(".","model/")
figures.dir     <-  file.path(".","figures/")

runName <- '00_NSAS_SST'

dir.create(file.path(results.dir,runName),showWarnings = FALSE)
dir.create(file.path(figures.dir,runName),showWarnings = FALSE)

results.dir     <-  file.path(".","results",runName)
figures.dir     <-  file.path(".","figures",runName)

source(file.path(function.dir,"cor test with autocor.r"))

startYear <- 1947
endYear   <- 2020

################################################
# read data
################################################

climate_output <- nc_open(file.path(data.dir,'SST','sst.mean.nc'))

# read sf assessment results
load(file.path(model.dir,'NSAS_sanoba_assessment_results_sf.RData'))

################################################
# clean SST data
################################################

climate_output

lon  <- ncvar_get(climate_output, varid = "lon")
lat  <- ncvar_get(climate_output, varid = "lat")
time <- ncvar_get(climate_output, varid = "time")

summary(lon)
summary(lat)
summary(time)

climate_output$dim$time$units
climate_output$dim$time$calendar

tas_time <- nc.get.time.series(climate_output, v = "sst",
                               time.dim.name = "time")
                               
                               
sst <- ncvar_get(climate_output, "sst")


la <-  which( is.element(lat,52:62))
lo <-  which( is.element(lon,c(0:10,357:360)) )
tim <- which( is.element(substr(tas_time,1,4),c(startYear:endYear)))

#take the subset of the array
NSsst <- sst[lo,la,tim]

dim(NSsst)
dimnames(NSsst) <-list(lon[lo],lat[la],tim)

NSsst2<-as.data.frame.table(NSsst)
names(NSsst2)    <-  c("long","lat","date","sst")
NSsst2$long <- as.numeric(as.character(NSsst2$long))
NSsst2$lat  <- as.numeric(as.character(NSsst2$lat))
NSsst2$date <- as.numeric(as.character(NSsst2$date))
NSsst2$sst  <- as.numeric(as.character(NSsst2$sst))
NSsst2$long[NSsst2$long >= 357 & NSsst2$long <= 360] <- NSsst2$long[NSsst2$long >= 357 & NSsst2$long <= 360]-360

NSsst2$time   <- tas_time[as.numeric(NSsst2$date)]
NSsst2$year   <- as.numeric(substr(NSsst2$time,1,4))
NSsst2$month  <- as.numeric(substr(NSsst2$time,6,7))

NSsst2$long[NSsst2$long==359] <- -1

# filter columns for final table
NSsst <- NSsst2[,c(1,2,6,7,4)]

# compute average over the area
NSsstmean <- aggregate(sst~year+month,NSsst , mean)

# merge with recruitment
rec <- as.data.frame(rec(NSH.sam))
ssb <- as.data.frame(ssb(NSH.sam))
rec <- rec[rec$year >= startYear & rec$year <= endYear,]
rec <- rec[,c('year','value')]
colnames(rec) <- c('year','rec')
ssb <- ssb[ssb$year >= startYear & ssb$year <= endYear,]
ssb <- ssb[,c('year','value')]
colnames(ssb) <- c('year','ssb')
NSAS_sst <- merge(NSsstmean,
                     rec,
                     all=T)
NSAS_sst <- merge(NSAS_sst,
                  ssb,
                  all=T)

# compute correlation for each month
cor.res <- data.frame(month = 1:12, cor =NA,up=NA,low=NA,pval=NA)

for(m in 1:12){
  subdats<-(subset(NSAS_sst, month == m))
  cortest <- with(subdats , cor.test.auto(sst,rec))
  cor.res$cor[m]  <- cortest$estimate
  cor.res$up[m]   <- cortest$conf.int[2]
  cor.res$low[m]  <- cortest$conf.int[1]
  cor.res$pval[m] <- cortest$p.value
}

# take the february march mean temperature
hist_sst <- subset(NSAS_sst , month %in% c(1,8,9))
hist_sst <- aggregate(cbind(sst,rec,ssb)~year , data = NSAS_sst , mean)
with(hist_sst , cor.test.auto(sst,rec))

mod <- lm(rec~sst,hist_sst)

hist_sst$sstResi <- (hist_sst$sst-mean(hist_sst$sst))/sd(hist_sst$sst)

################################################
# saving obj
################################################
save(hist_sst,file = file.path(data.dir,'SST_NSAS.RData'))

write.csv(x = hist_sst,
          file = file.path(results.dir,'SST_NSAS.csv'),row.names = FALSE)

################################################
# plotting
################################################

# SST lat/lon
scaling_factor <- 1
png(file.path(figures.dir,'SST_lat_long.png'), 
    width = 12*scaling_factor, height = 12*scaling_factor, units = "cm", res = 300, pointsize = 10)

p <- ggplot(data = NSsst , aes(year + (month-1)/12 , sst)) + 
      geom_line() + 
      facet_grid(long~lat,scales = 'free_y')

print(p)
dev.off()

# SST month
scaling_factor <- 1
png(file.path(figures.dir,'SST_month.png'), 
    width = 12*scaling_factor, height = 12*scaling_factor, units = "cm", res = 300, pointsize = 10)

p <- ggplot(data = NSsstmean , aes(year , sst)) + 
      geom_line() + 
      facet_wrap(.~month,scales = 'free')+
  theme(axis.text.x = element_text(angle = 90))

print(p)
dev.off()

# SST vs rec
scaling_factor <- 1
png(file.path(figures.dir,'SST_rec.png'), 
    width = 12*scaling_factor, height = 12*scaling_factor, units = "cm", res = 300, pointsize = 10)

p <- ggplot(NSAS_sst , aes(sst,rec)) + 
      geom_point() + 
      facet_wrap(~month,scales= "free")  +  
      geom_smooth(model = lm)

print(p)
dev.off()

scaling_factor <- 1
png(file.path(figures.dir,'SST_corr.png'), 
    width = 12*scaling_factor, height = 12*scaling_factor, units = "cm", res = 300, pointsize = 10)

p <- ggplot(cor.res) + 
      geom_pointrange(aes(x=month,y=cor,ymin=low,ymax=up),col="blue")+
      geom_hline(yintercept =0)

print(p)
dev.off()

scaling_factor <- 1
png(file.path(figures.dir,'SST_corr_subset.png'), 
    width = 12*scaling_factor, height = 12*scaling_factor, units = "cm", res = 300, pointsize = 10)

p <- ggplot(data= hist_sst , aes(sstResi ,rec) )+
      geom_line()+
      geom_smooth(method = "lm")+ 
      ggtitle( paste("Rec =",round(coef(mod)[1],0),round(coef(mod)[2],0),"x temp" ))+
      xlab("mean SST (Jan/Aug/Sep)")
  
print(p)
dev.off()

scaling_factor <- 1
png(file.path(figures.dir,'SST_resi.png'), 
    width = 12*scaling_factor, height = 12*scaling_factor, units = "cm", res = 300, pointsize = 10)

p <- ggplot(hist_sst,aes(x=year,y=sstResi))+
      geom_point()+
      geom_line()

print(p)
dev.off()