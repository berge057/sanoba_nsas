
#data source :
#https://psl.noaa.gov/cgi-bin/db_search/DBSearch.pl?Dataset=ICOADS+2-degree+Standard&Dataset=ICOADS+2-degree+Enhanced&Variable=Sea+Surface+Temperature
#
library(ncdf4)
library(ncdf4.helpers)

setwd('C:/git/sanoba_nsas/data/SST/temp data')

climate_filepath <- paste0("sst.mean.nc")
climate_output <- nc_open(climate_filepath)


climate_output

lon  <- ncvar_get(climate_output, varid = "lon")
lat  <- ncvar_get(climate_output, varid = "lat")
time <- ncvar_get(climate_output, varid = "time")

summary(lon)
summary(lat)
summary(time)

climate_output$dim$time$units
climate_output$dim$time$calendar

tas_time <- nc.get.time.series(climate_output, v = "sst",
                               time.dim.name = "time")
                               
                               
sst <- ncvar_get(climate_output, "sst")


la <-  which( is.element(lat,52:58))
lo <-  which( is.element(lon,c(0:8,358:360)) )
tim <- which( is.element(substr(tas_time,1,4),c(1956:2019)) )

#take the subset of the array
# dim(sst)
NSsst <- sst[lo,la,tim]


dim(NSsst)
dimnames(NSsst) <-list(lon[lo],lat[la],tim)

NSsst2<-as.data.frame.table(NSsst)
names(NSsst2)    <-  c("long","lat","date","sst")
NSsst2[,1]  <- as.character(NSsst2[,1])
NSsst2[,2]  <- as.character(NSsst2[,2])
NSsst2[,3]  <- as.character(NSsst2[,3])

NSsst2$time <- tas_time[as.numeric(NSsst2$date)]
NSsst2$year <- substr(NSsst2$time,1,4)
NSsst2$month <- substr(NSsst2$time,6,7)

NSsst2$long[NSsst2$long==359] <- -1

NSsst <- NSsst2 [,c(1,2,6,7,4)]

NSsst[,1]  <- as.numeric(NSsst[,1])
NSsst[,2]  <- as.numeric(NSsst[,2])
NSsst[,3]  <- as.numeric(NSsst[,3])
NSsst[,4]  <- as.numeric(NSsst[,4])


library(ggplot2)
ggplot(data = NSsst , aes(year + (month-1)/12 , sst)) + geom_line() + facet_grid(long~lat)

# compute average over the area
NSsstmean <- aggregate(sst~year+month,NSsst , mean)


pdf("temp and plaice recruitment.pdf")
ggplot(data = NSsstmean , aes(year , sst)) + geom_line() + facet_wrap(.~month)

# load the plaice data
library(FLCore)
load("PLE-NS.RData" )
rec<-as.data.frame(rec(stock),drop=T)
names(rec)[2]<- "rec"
# shift year so that year correspond to YC not to age 1
rec$year <-  rec$year - 1


dats <- merge(NSsstmean,rec,all=T)

# look at correlation
ggplot(dats , aes(sst,rec)) + geom_point() + facet_wrap(~month,scales= "free")  +  geom_smooth(model = lm)
source("cor test with autocor.r")

cor.res <- data.frame(month = 1:12, cor =NA,up=NA,low=NA,pval=NA)

for ( m in 1:12)
{
subdats<-(subset(dats, month == m))
cortest <- with(subdats , cor.test.auto(sst,rec))
cor.res$cor[m]  <- cortest$estimate
cor.res$up[m]   <- cortest$conf.int[2]
cor.res$low[m]  <- cortest$conf.int[1]
cor.res$pval[m] <- cortest$p.value
}


ggplot(cor.res) + geom_pointrange(aes(x=month,y=cor,ymin=low,ymax=up),col="blue") +
geom_hline(yintercept =0)

# take the february march mean temperature
winter <- subset(dats , month %in% c(2,3))
winter <- aggregate(cbind(sst,rec)~year , data = winter , mean)
with (winter , cor.test.auto(sst,rec))

mod <- lm(rec~sst,winter)

ggplot(data= winter , aes(sst ,rec) ) +geom_line()  +
  geom_smooth(method = "lm", se = FALSE) + ggtitle( paste("Rec =",round(coef(mod)[1],0),round(coef(mod)[2],0),"x temp" ))+
  xlab("mean SST february March")
  
   dev.off()