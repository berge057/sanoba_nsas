
#data source :
#https://psl.noaa.gov/cgi-bin/db_search/DBSearch.pl?Dataset=ICOADS+2-degree+Standard&Dataset=ICOADS+2-degree+Enhanced&Variable=Sea+Surface+Temperature
#
library(ncdf4)
library(ncdf4.helpers)
library(ggplot2)
library(FLCore)
library(tidyverse)

rm(list=ls())

#path <- "J:/git/sanoba_nsas"
path <- "C:/git/sanoba_nsas"

try(setwd(path),silent=TRUE)

################################################
# setup paths
################################################

data.dir        <- file.path(".","data")
script.dir      <-  file.path(".","side_scripts/")
function.dir    <-  file.path(".","functions/")
results.dir     <-  file.path(".","results/")
model.dir       <-  file.path(".","model/")
figures.dir     <-  file.path(".","figures/")

runName <- '00_NSAS_SST'

dir.create(file.path(results.dir,runName),showWarnings = FALSE)
dir.create(file.path(figures.dir,runName),showWarnings = FALSE)

results.dir     <-  file.path(".","results",runName)
figures.dir     <-  file.path(".","figures",runName)

source(file.path(function.dir,"cor test with autocor.r"))

load(file.path(data.dir,'SST_NSAS.RData'))

startYear <- 1900
endYear   <- 2150

#sdNorm <- 0.5220676
#meanNorm <- 10.08569

rcpMat <- c('rcp45','rcp85')

rcp_proj_sst <- list()

for(rcp in rcpMat){
    
  fileMat <- list.files(file.path(data.dir,'SST',rcp))
  flagFirst <- TRUE
  
  for(idxFile in fileMat){
    print(idxFile)
    climate_output <- nc_open(file.path(data.dir,'SST',rcp,idxFile))
    
    lon  <- ncvar_get(climate_output, varid = "lon")
    lat  <- ncvar_get(climate_output, varid = "lat")
    time <- ncvar_get(climate_output, varid = "time")
    
    tas_time <- nc.get.time.series(climate_output, v = "tos",
                                   time.dim.name = "time")
    
    
    #sst <- ncvar_get(climate_output, "sst")
    sst <- ncvar_get(climate_output, "tos")
    
    
    
    #la <-  which( is.element(lat,52:62))
    la <-  which(52 < lat & lat < 62)
    lo <-  which(-3 < lon & lon < 10)
    
    idxLa <- la[which(la %in% lo)]
    #idxLo <- which(lo %in% la)
    #length(lat[la[idxLa]])
    #length(lon[lo[idxLo]])
    
    r = ((idxLa-1) %% dim(lat)[1]) + 1
    c = floor((idxLa-1) / dim(lat)[1]) + 1
    
    tim <- which( is.element(substr(tas_time,1,4),c(startYear:endYear)))
    
    #take the subset of the array
    NSsst <- sst[r,c,tim]-273.15
    
    dimnames(NSsst) <-list(lon[r,c[1]],lat[r[1],c],tim)
    
    NSsst2<-as.data.frame.table(NSsst)
    names(NSsst2)    <-  c("long","lat","date","sst")
    NSsst2$long <- as.numeric(as.character(NSsst2$long))
    NSsst2$lat  <- as.numeric(as.character(NSsst2$lat))
    NSsst2$date <- as.numeric(as.character(NSsst2$date))
    NSsst2$sst  <- as.numeric(as.character(NSsst2$sst))#-1.613023
    
    NSsst2$time   <- tas_time[as.numeric(NSsst2$date)]
    NSsst2$year   <- as.numeric(substr(NSsst2$time,1,4))
    NSsst2$month  <- as.numeric(substr(NSsst2$time,6,7))
    
    NSsst <- NSsst2[,c(1,2,6,7,4)]
    
    # compute average over the area
    NSsstmean <- aggregate(sst~year+month,NSsst , mean)
    
    if(flagFirst == TRUE){
      month_subset_proj <- subset(NSsstmean , month %in% c(1,8,9))
      NSsstmean_proj <- NSsstmean
      flagFirst <- FALSE
    }else{
      month_subset_proj <- rbind(month_subset_proj,subset(NSsstmean , month %in% c(1,8,9)))
      NSsstmean_proj      <- rbind(NSsstmean_proj,NSsstmean)
    }
  }
  
  temp          <- aggregate(sst~year , data = month_subset_proj, mean)
  temp$sst      <- temp$sst + (mean(hist_sst$sst[hist_sst$year %in% temp$year])-mean(temp$sst[temp$year %in% hist_sst$year]))
  temp$sstResi  <- (temp$sst-mean(hist_sst$sst))/sd(hist_sst$sst)
  
  #ggplot()+
  #  geom_line(data=temp,aes(x=year,y=sst),col='blue')+
  #  geom_line(data=hist_sst,aes(x=year,y=sst),col='red')
  
  #ggplot()+
  #  geom_line(data=temp,aes(x=year,y=sstResi),col='blue')+
  #  geom_line(data=hist_sst,aes(x=year,y=sstResi),col='red')
  
  rcp_proj_sst[[rcp]] <- temp
  
  ################################################
  # saving table
  ################################################
  write.csv(x = rcp_proj_sst[[rcp]],
            file = file.path(results.dir,paste0('SST_NSAS_',rcp,'.csv')),row.names = FALSE)  
}

################################################
# saving obj
################################################
save(rcp_proj_sst,
     file = file.path(data.dir,'SST_NSAS_rcp.RData'))

################################################
# plotting all
################################################

flagFirst <- TRUE

rcpNames <- names(rcp_proj_sst)

for(idx in 1:length(rcp_proj_sst)){
  if(idx == 1){
    df.rcp      <- as.data.frame(rcp_proj_sst[[idx]])
    df.rcp      <- pivot_longer(!year,data = df.rcp,names_to = "metric",values_to = 'data')
    df.rcp$rcp  <- rcpNames[idx]
  }else{
    df.rcp.temp       <- as.data.frame(rcp_proj_sst[[idx]])
    df.rcp.temp       <- pivot_longer(!year,data = df.rcp.temp,names_to = "metric",values_to = 'data')
    df.rcp.temp$rcp   <- rcpNames[idx]
    df.rcp            <- rbind(df.rcp.temp,df.rcp)
  }
}

df.hist_sst <- pivot_longer(!year,data = hist_sst,names_to = "metric",values_to = 'data')
df.hist_sst <- subset(df.hist_sst,metric == 'sst' | metric == 'sstResi')
df.hist_sst$rcp <- 'hist'

df.rcp <- rbind(df.rcp,df.hist_sst)

################################################
# plotting
################################################

scaling_factor <- 1
png(file.path(figures.dir,'SST_rcp.png'), 
    width = 12*scaling_factor, height = 12*scaling_factor, units = "cm", res = 300, pointsize = 10)

p <- ggplot(subset(df.rcp,metric=='sstResi'),aes(x=year,y=data,col=rcp))+
      geom_line()+
      geom_vline(xintercept = 2021)+
      ylab('sstResi')

print(p)
dev.off()
