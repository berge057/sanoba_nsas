#-------------------------------------------------------------------------------
# WKNSMSE
#
# Author: Benoit Berges
#         WMR, The Netherland
# email: benoit.berges@wur.nl
#
#  MSE of North Sea Herring
#
# Date: 2018/11/18
#
# Build for R3.5.1, 64bits
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
# 1) load packages 
#    setup paths 
#    load functions
#-------------------------------------------------------------------------------

rm(list=ls())

library(minpack.lm)  # install.packages("minpack.lm")
library(stats)
require(reshape2)
require(ggplot2)
library(gridExtra)
library(RColorBrewer)
library(grid)
library(FLSAM)

assessment_name   <- "NSAS_sanoba"

# paths to different subfolders
dataPath      <- file.path(".","data/")
modelPath     <- file.path(".","model/")
resultsPath   <- file.path(".","results/")
scriptPath    <- file.path(".","side_scripts/")
functionPath  <- file.path(".","functions/")
figurePath    <- file.path(".","figures/")

nits <- 1000

outputName <- paste0('grid_refPoints_',nits,'its')

plotIndRIsk     <- TRUE

#-------------------------------------------------------------------------------
# 2) plotting grid search for different cases
#-------------------------------------------------------------------------------

# define string array for f01 and f26
f01         <- ac(0:1)
f26         <- ac(2:6)

  # choose HCR combination to plot
  #HCRPlot <- 'A_IAV_A_BB_A'
  #HCRPlot <- 'A_IAV_AB_BB_AB'
  #HCRPlot <- 'A'
  #HCRPlot <- 'B_IAV_A_BB_A'
  #HCRPlot <- 'B_IAV_AB_BB_AB'
  #HCRPlot <- 'B'
  

fileList <- list.files(file.path(modelPath,outputName),pattern = "\\.RData$")

load(file.path(modelPath,outputName,fileList[1]))

Ftar                <- array(NA, dim=c(1,length(fileList)))
Btrig               <- array(NA, dim=c(1,length(fileList)))
LTY                 <- array(NA, dim=c(1,length(fileList)))
LTR1                <- array(NA, dim=c(1,length(fileList)))
LTR3                <- array(NA, dim=c(1,length(fileList)))
LTF01               <- array(NA, dim=c(1,length(fileList)))
LTF26               <- array(NA, dim=c(1,length(fileList)))
LT_RiskTrend        <- array(NA, dim=c(1,length(fileList)))
LT_RiskTrendSig     <- array(NA, dim=c(1,length(fileList)))
LT_SSBTailTrend     <- array(NA, dim=c(1,length(fileList)))
LT_SSBTailTrendSig  <- array(NA, dim=c(1,length(fileList)))

#metricsPeriod <- projPeriod[(length(projPeriod)-10):(length(projPeriod)-1)]
metricsPeriod <- ac(2031:2041)

for(idxFile in 1:length(fileList)){
  print(fileList[idxFile])
  
  load(file.path(modelPath,outputName,fileList[idxFile]))

  fileNameSplit <- strsplit(fileList[idxFile],"_")
  
  Ftar[idxFile]   <- as.numeric(fileNameSplit[[1]][4])
  Btrig[idxFile]  <- as.numeric(fileNameSplit[[1]][3])
  
  biol@catch    <- computeCatch(biol)
  biol@stock    <- computeStock(biol)
  biol@landings <- computeLandings(biol)
  
  # risk of SSB < Blim
  SSB <- ssb(biol[,metricsPeriod])
  SSB <- drop(SSB)
  
  SSB_riskMat <- array(FALSE,dim=dim(SSB))
  SSB_bool    <- array(FALSE,dim=c(1,nits))
  
  
  for(idxIter in 1:nits){
    # store value per year
    SSB_riskMat[which(SSB[,idxIter] < referencePoints$Blim),idxIter] <- TRUE
    
    # TRUE/FALSE for each iteration
    if(length(which(SSB[,idxIter] < referencePoints$Blim)!=0))
      SSB_bool[idxIter] <- TRUE
  }
  
  SSB_prob <- array(NA,dim=c(1,length(metricsPeriod)))
  
  for(idxProb in 1:length(metricsPeriod)){
    SSB_prob[idxProb] <- length(which(SSB_riskMat[idxProb,] == TRUE))/nits
  }
  
  SSBTail     <- apply(SSB, 1, quantile, probs=c(0.05, 0.5, 0.95), na.rm=TRUE)
  lmFrame <- as.data.frame(t(rbind(an(metricsPeriod),SSB_prob,SSBTail[1,])))
  colnames(lmFrame) <- c('years','risk','SSB5per')
  
  # trend in annual risk
  riskLR <- lm(risk~years,data=lmFrame)
  
  # trend in SSB 5% tail
  SSBTailLR <- lm(SSB5per~years,data=lmFrame)
  
  if(plotIndRIsk == TRUE){
    
    sink(file.path(resultsPath,paste0('lm_ftar_',ftarget,'_btrig_',btrigger,'.txt')))
    print('annual risk')
    print(summary(riskLR))
    print('5% tail')
    print(summary(SSBTailLR))
    print('slope as % of average')
    print(SSBTailLR$coefficients[2]/mean(lmFrame$SSB5per))
    print('Ftar/Btrig')
    print(c(ftarget,btrigger))
    sink()
    
    
    scaling_factor <- 1.5
    png(file.path(figurePath,paste0('05_',outputName),paste0(assessment_name,'_trends_ftar_',ftarget,'_btrig_',btrigger,'.png')), 
        width = 12*scaling_factor, height = 8*scaling_factor, units = "cm", res = 300, pointsize = 10)
    
    # plot
    par(mfrow=c(2,1))
    plot(metricsPeriod,SSBTail[2,],type='l',
         ylim=c(0,max(SSBTail[3,])),
         ylab='SSB',
         xlab='years',main=paste0('ftar_',ftarget,'_btrig_',btrigger))
    lines(metricsPeriod,SSBTail[1,],type='l',col='blue',lty=2)
    lines(metricsPeriod,SSBTail[3,],type='l',col='blue',lty=2)
    
    plot(metricsPeriod,SSB_prob,type='l',
         ylim=c(0,0.08),
         ylab='Annual risk',
         xlab='years',
         xlim=c(an(min(metricsPeriod)),an(max(metricsPeriod))))
    lines(c(min(metricsPeriod),max(metricsPeriod)),c(0.05,0.05),lty=2,col='green')
    
    dev.off()
  }
  
  
  # Fbar
  FHCR26      <- FHCR[f26,ac(2021:2041)]
  FHCR01      <- FHCR[f01,ac(2021:2041)]
  
  f01Mat <- apply(drop(FHCR01[,metricsPeriod]),c(2,3),'mean')
  f26Mat <- apply(drop(FHCR26[,metricsPeriod]),c(2,3),'mean')
  
  f01Quant <- apply(f01Mat, 1, quantile, probs=c(0.05, 0.5, 0.95), na.rm=TRUE)
  f26Quant <- apply(f26Mat, 1, quantile, probs=c(0.05, 0.5, 0.95), na.rm=TRUE)
  
  
  #
  TACBMat <- drop(TAC[,metricsPeriod,,,"B"])
  
  TACBQuant <- apply(TACBMat, 1, quantile, probs=c(0.05, 0.5, 0.95), na.rm=TRUE)
  
  mean(TACBQuant['50%',])
  
  mean(TAC[,,,,c("B")], na.rm=1)

  # LTY
  catchQuant  <- apply(drop(biol[,metricsPeriod]@catch), 1, quantile, probs=c(0.05, 0.5, 0.95), na.rm=TRUE)
  
  # store value for each file
  LTR1[idxFile]                   <- mean(SSB_prob) #length(which(SSB_bool))/nits
  LTR3[idxFile]                   <- max(SSB_prob) #length(which(SSB_bool))/nits
  LTY[idxFile]                    <- mean(catchQuant['50%',])
  LTF01[idxFile]                  <- mean(f01Quant['50%',])
  LTF26[idxFile]                  <- mean(f26Quant['50%',])
  LT_RiskTrend[idxFile]           <- summary(riskLR)$coefficients[2,1]
  LT_RiskTrendSig[idxFile]        <- ifelse(summary(riskLR)$coefficients[2,4] > 0.05,0,1) # 0 is not significant. 1 is significant
  LT_SSBTailTrend[idxFile]        <- summary(SSBTailLR)$coefficients[2,1]
  LT_SSBTailTrendSig[idxFile]     <- ifelse(summary(SSBTailLR)$coefficients[2,4] > 0.05,0,1) # 0 is not significant. 1 is significant
}

FtarUnique    <- unique(t(Ftar))
FtarUnique    <- sort(FtarUnique)
BtrigUnique   <- unique(t(Btrig))
BtrigUnique   <- sort(BtrigUnique)

dFtar   <- 0.01
dBtrig  <- 0.1*1e6
FtarSeq   <- seq(0.2,0.34,dFtar)
BtrigSeq  <- seq(0.9*1e6,1.4*1e6,dBtrig)

LTYMat      <- array(NA, dim=c(length(FtarSeq),length(BtrigSeq)),dimnames = list(FtarSeq,BtrigSeq)) # long term yield
LTR1Mat     <- array(NA, dim=c(length(FtarSeq),length(BtrigSeq)),dimnames = list(FtarSeq,BtrigSeq)) # long term risk
LTR3Mat     <- array(NA, dim=c(length(FtarSeq),length(BtrigSeq)),dimnames = list(FtarSeq,BtrigSeq)) # long term risk
LTF01Mat    <- array(NA, dim=c(length(FtarSeq),length(BtrigSeq)),dimnames = list(FtarSeq,BtrigSeq)) # long realised F01
LTF26Mat        <- array(NA, dim=c(length(FtarSeq),length(BtrigSeq)),dimnames = list(FtarSeq,BtrigSeq)) # long realised F26
LT_RiskTrendMat    <- array(NA, dim=c(length(FtarSeq),length(BtrigSeq)),dimnames = list(FtarSeq,BtrigSeq)) # long realised F26
LT_RiskTrendSigMat    <- array(NA, dim=c(length(FtarSeq),length(BtrigSeq)),dimnames = list(FtarSeq,BtrigSeq)) # long realised F26
LT_SSBTailTrendMat    <- array(NA, dim=c(length(FtarSeq),length(BtrigSeq)),dimnames = list(FtarSeq,BtrigSeq)) # long realised F26
LT_SSBTailTrendSigMat    <- array(NA, dim=c(length(FtarSeq),length(BtrigSeq)),dimnames = list(FtarSeq,BtrigSeq)) # long realised F26

for(idxFtar in 1:length(FtarUnique)){
  for(idxBtrig in 1:length(BtrigUnique)){
    idxMatch <- which((Ftar %in% FtarUnique[idxFtar]) & (Btrig %in% BtrigUnique[idxBtrig]))
    
    if(length(idxMatch)!=0){
      LTYMat[ac(FtarUnique[idxFtar]),
             ac(BtrigUnique[idxBtrig])]    <- LTY[idxMatch]
      LTR1Mat[ac(FtarUnique[idxFtar]),
              ac(BtrigUnique[idxBtrig])]   <- LTR1[idxMatch]
      LTR3Mat[ac(FtarUnique[idxFtar]),
              ac(BtrigUnique[idxBtrig])]   <- LTR3[idxMatch]
      LTF01Mat[ac(FtarUnique[idxFtar]),
               ac(BtrigUnique[idxBtrig])]  <- LTF01[idxMatch]
      LTF26Mat[ac(FtarUnique[idxFtar]),
               ac(BtrigUnique[idxBtrig])]  <- LTF26[idxMatch]
      # trend matrices
      LT_RiskTrendMat[ac(FtarUnique[idxFtar]),
                      ac(BtrigUnique[idxBtrig])]        <- LT_RiskTrend[idxMatch]
      LT_RiskTrendSigMat[ac(FtarUnique[idxFtar]),
                         ac(BtrigUnique[idxBtrig])]     <- LT_RiskTrendSig[idxMatch]
      LT_SSBTailTrendMat[ac(FtarUnique[idxFtar]),
                         ac(BtrigUnique[idxBtrig])]     <- LT_SSBTailTrend[idxMatch]
      LT_SSBTailTrendSigMat[ac(FtarUnique[idxFtar]),
                            ac(BtrigUnique[idxBtrig])]  <- LT_SSBTailTrendSig[idxMatch]
    }
  }
}

###################################### Plotting ################################

# create data frame for plotting
plotMat <- as.data.frame(cbind(melt(LTYMat),
                               melt(LTR1Mat)$value,
                               melt(LTR3Mat)$value,
                               melt(LTF01Mat)$value,
                               melt(LTF26Mat)$value))
colnames(plotMat)   <- c("Ftarget","Btrigger","LTY","LTR1","LTR3",'LTRF01','LTRF26')
plotRowNames        <- ac(round(plotMat$LTY))
plotRowNames[which(is.na(plotRowNames))] <- paste0(plotRowNames[which(is.na(plotRowNames))],ac(1:length(which(is.na(plotRowNames)))))
rownames(plotMat)   <- plotRowNames

#myPalette <- colorRampPalette(brewer.pal(11, "RdYlGn"))

myPalette1 <- colorRampPalette(c("red", "green", "blue"),
                               space = "rgb")

myPalette2 <- colorRampPalette(c("red", "blue",'yellow'),
                               space = "rgb")

# long term yield
plotLabelsLTY    <- ac(round(plotMat$LTY/1e03))

treshold_low_LTY  <- 4*1e5#/1e03
treshold_high_LTY <- 4.7*1e5#/1e03

# tresholding for clarity if needed
#plotMat$LTY[plotMat$LTY < treshold_low]   <- treshold_low
#plotMat$LTY[plotMat$LTY > treshold_high]  <- treshold_high

p1 <- ggplot(plotMat,aes(x=Btrigger, y=Ftarget, fill=LTY,label=plotLabelsLTY))+ 
      geom_tile(aes(x=Btrigger, y=Ftarget, fill=LTY))+
      scale_fill_gradientn(name='Long term yield',colours = myPalette2(4),
                           limits=c(treshold_low_LTY,treshold_high_LTY),na.value="white")+
      xlab('Btrigger') + ylab('Ftarget')+
      scale_x_continuous(breaks=BtrigSeq) + scale_y_continuous(breaks=FtarSeq)+
      geom_text()+
      theme( panel.grid.major = element_blank(), 
             panel.grid.minor = element_blank(),
             panel.background = element_blank(), 
             panel.border = element_blank())+
      geom_vline(xintercept=BtrigSeq-dBtrig/2)+
      geom_hline(yintercept=FtarSeq-dFtar/2)

print(p1)

# Long term risk 3
plotLabelsLTR3    <- ac(round(plotMat$LTR3*1e4)/1e4)
treshold_low_LTR  <- 0.03
treshold_high_LTR <- 0.07

# tresholding for clarity if needed
plotMat$LTR3[plotMat$LTR3 < treshold_low_LTR]   <- treshold_low_LTR
plotMat$LTR3[plotMat$LTR3 > treshold_high_LTR]  <- treshold_high_LTR

p2 <- ggplot(plotMat,aes(x=Btrigger, y=Ftarget, fill=LTR3,label=plotLabelsLTR3))+
      geom_tile(aes(x=Btrigger, y=Ftarget, fill=LTR3))+
      scale_fill_gradientn(name='Risk 3',colours = rev(myPalette1(6)),
                           limits=c(treshold_low_LTR,treshold_high_LTR),na.value="white")+
      xlab('Btrigger') + ylab('Ftarget')+
      scale_x_continuous(breaks=BtrigSeq) + scale_y_continuous(breaks=FtarSeq)+
      geom_text()+
      theme( panel.grid.major = element_blank(), 
             panel.grid.minor = element_blank(),
             panel.background = element_blank(), 
             panel.border = element_blank())+
      geom_vline(xintercept=BtrigSeq-dBtrig/2)+
      geom_hline(yintercept=FtarSeq-dFtar/2)

scaling_factor <- 2
png(file.path(figurePath,paste0('05_',outputName),paste0(assessment_name,'_grid.png')), 
    width = 12*scaling_factor, height = 8*scaling_factor, units = "cm", res = 300, pointsize = 10)

p <- grid.arrange(p1, p2)

print(p)
dev.off()
