

# function to calculated cor test with autocor according to modified chelton method

cor.test.auto<- function(var1,var2){
varr<-na.omit(data.frame(var1,var2))
autocor1<-acf(varr$var1,plot=F)$acf[2]
autocor2<-acf(varr$var2,plot=F)$acf[2]

ress<-list()
r<-cor.test(var1,var2,data=varr,na.action="omit" )$estimate
ress$estimate<-r

N<-dim(varr)[1]-2
N2<-1/(1/N+(2/N)*autocor1*autocor2)
ress$parameter<-N2

zr<-0.5*log((1+r)/(1-r))
zmin<-zr-1.96*(1/(N2-1))^0.5
zmax<-zr+1.96*(1/(N2-1))^0.5

ress$con.int<-c(0,0)
ress$conf.int[1]<-(exp(2*zmin)-1)/(exp(2*zmin)+1)
ress$conf.int[2]<-(exp(2*zmax)-1)/(exp(2*zmax)+1)

tt<-(N2^0.5)*((r^2)/(1-(r^2)))^0.5

ress$p.value<-pt(-tt,N2)*2

return(ress)

}

panel.cor <- function(x, y, digits=2, prefix="", cex.cor)
{
   if(sum(!is.na(x) & !is.na(y))>2)
   { usr <- par("usr"); on.exit(par(usr))
    par(usr = c(0, 1, 0, 1))
    r <- abs(cor.test(x, y)$estimate)
    txt <- format(c(r, 0.123456789), digits=digits)[1]
    txt <- paste(prefix, txt, sep="")
    if(missing(cex.cor)) cex <- 0.8/strwidth(txt)

    test <- cor.test.auto(x,y)
    # borrowed from printCoefmat
    Signif <- symnum(test$p.value, corr = FALSE, na = FALSE,
                  cutpoints = c(0, 0.001, 0.01, 0.05, 0.1, 1),
                  symbols = c("***", "**", "*", ".", " "))

    text(0.5, 0.5, txt, cex = cex * r)
    text(.8, .8, Signif, cex=cex, col=2)
}
}