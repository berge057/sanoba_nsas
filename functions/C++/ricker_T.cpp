#include <TMB.hpp>
 
template<class Type>
Type objective_function<Type>::operator() ()
{
	DATA_VECTOR(SSB_pred)
	DATA_VECTOR(SST_pred)
	DATA_VECTOR(SSB)
	DATA_VECTOR(SST)
	DATA_VECTOR(logR);

	PARAMETER(logA);
	PARAMETER(logB);
	PARAMETER(logC);
	PARAMETER(logSigma);
	vector<Type> pred_model=logA+log(SSB)-exp(logB)*SSB+exp(logC)*SST;
	Type nl=-sum(dnorm(logR,pred_model,exp(logSigma),true));
	
	matrix<Type> pred(SSB_pred.size(),SST_pred.size());
	/*pred=logA+log(SSB_pred)-exp(logB)*SSB_pred+exp(logC)*SST_pred;*/
	for(int iSST=0; iSST<SST_pred.size(); ++iSST){
		for(int iSSB=0; iSSB<SSB_pred.size(); ++iSSB){
			pred(iSSB,iSST)=logA+log(SSB_pred(iSSB))-exp(logB)*SSB_pred(iSSB)+exp(logC)*SST_pred(iSST);
		}
	}
	ADREPORT(pred)
	/*ADREPORT(pred_model)*/
	return nl;
}
