#include <TMB.hpp>
 
template<class Type>
Type objective_function<Type>::operator() ()
{
	DATA_VECTOR(SSB)
	DATA_VECTOR(logR);

	PARAMETER(logA); 
	PARAMETER(logB);
	PARAMETER(logSigma);
	
	vector<Type> pred_model=exp(logA)*SSB;
	if(SSB < exp(logB)){
		vector<Type> pred_model=exp(logA)*SSB;
	}else{
		vector<Type> pred_model=exp(logA)*exp(logB)*SSB;
	}
	Type nl=-sum(dnorm(logR,pred_model,exp(logSigma),true));
	ADREPORT(pred_model)
	return nl;
}
