#include <TMB.hpp>
 
template<class Type>
Type objective_function<Type>::operator() ()
{
	DATA_VECTOR(SSB)
	DATA_VECTOR(SST)
	DATA_VECTOR(logR);

	PARAMETER(logA);
	PARAMETER(logB);
	PARAMETER(logC);
	PARAMETER(logSigma);
	vector<Type> pred_model=logA+log(1-exp(logC)*SST)+log(SSB)-exp(logB)*SSB;
	Type nl=-sum(dnorm(logR,pred_model,exp(logSigma),true));
	ADREPORT(pred_model)
	/*ADREPORT(pred_model)*/
	return nl;
}
