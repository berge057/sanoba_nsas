#include <TMB.hpp>
 
template<class Type>
Type objective_function<Type>::operator() ()
{
	DATA_VECTOR(SSB_pred)
	DATA_VECTOR(SST_pred)
	DATA_VECTOR(SSB)
	DATA_VECTOR(SST)
	DATA_VECTOR(logR);

	PARAMETER(logA);
	PARAMETER(logB);
	PARAMETER(logC);
	PARAMETER(logSigma);
	vector<Type> pred_model=logA+log(SSB)-exp(logB)*SSB+exp(logC)*SST;
	Type nl=-sum(dnorm(logR,pred_model,exp(logSigma),true));
	vector<Type> pred=logA+log(SSB_pred)-exp(logB)*SSB_pred+exp(logC)*SST_pred;
	ADREPORT(pred)
	/*ADREPORT(pred_model)*/
	return nl;
}
