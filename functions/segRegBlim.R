segRegBlim <- function () 
{
  logl <- function(a, rec, ssb) {
    loglAR1(log(rec), FLQuant(log(ifelse(c(ssb) <= 874198, a * 
                                           c(ssb), a * 874198)), dimnames = dimnames(ssb)))
  }
  model <- rec ~ FLQuant(ifelse(c(ssb) <= 874198, a * c(ssb), a * 
                                  874198), dimnames = dimnames(ssb))
  initial <- structure(function(rec, ssb) {
    return(FLPar(a = median(c(rec)/c(ssb), na.rm = TRUE)))
  }, lower = rep(0, 0), upper = rep(Inf, 2))
  return(list(logl = logl, model = model, initial = initial))
}