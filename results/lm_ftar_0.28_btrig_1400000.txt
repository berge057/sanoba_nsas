[1] "annual risk"

Call:
lm(formula = risk ~ years, data = lmFrame)

Residuals:
      Min        1Q    Median        3Q       Max 
-0.015591 -0.006295 -0.002500  0.008818  0.016318 

Coefficients:
              Estimate Std. Error t value Pr(>|t|)
(Intercept) -1.9007273  2.0645476  -0.921    0.381
years        0.0009545  0.0010140   0.941    0.371

Residual standard error: 0.01064 on 9 degrees of freedom
Multiple R-squared:  0.08963,	Adjusted R-squared:  -0.01152 
F-statistic: 0.8861 on 1 and 9 DF,  p-value: 0.3711

[1] "5% tail"

Call:
lm(formula = SSB5per ~ years, data = lmFrame)

Residuals:
   Min     1Q Median     3Q    Max 
-83122 -33003   9634  23331  69009 

Coefficients:
            Estimate Std. Error t value Pr(>|t|)
(Intercept) 11401615    9119042    1.25    0.243
years          -5150       4479   -1.15    0.280

Residual standard error: 46980 on 9 degrees of freedom
Multiple R-squared:  0.1281,	Adjusted R-squared:  0.03121 
F-statistic: 1.322 on 1 and 9 DF,  p-value: 0.2798

[1] "slope as % of average"
       years 
-0.005621472 
[1] "Ftar/Btrig"
[1] 2.8e-01 1.4e+06
