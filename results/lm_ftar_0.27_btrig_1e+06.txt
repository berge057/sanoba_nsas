[1] "annual risk"

Call:
lm(formula = risk ~ years, data = lmFrame)

Residuals:
      Min        1Q    Median        3Q       Max 
-0.006946 -0.002805 -0.001518  0.003836  0.008336 

Coefficients:
              Estimate Std. Error t value Pr(>|t|)
(Intercept)  1.2021636  0.9788465   1.228    0.251
years       -0.0005727  0.0004808  -1.191    0.264

Residual standard error: 0.005042 on 9 degrees of freedom
Multiple R-squared:  0.1362,	Adjusted R-squared:  0.04023 
F-statistic: 1.419 on 1 and 9 DF,  p-value: 0.264

[1] "5% tail"

Call:
lm(formula = SSB5per ~ years, data = lmFrame)

Residuals:
   Min     1Q Median     3Q    Max 
-32683  -5685   -169   7822  20248 

Coefficients:
            Estimate Std. Error t value Pr(>|t|)
(Intercept) -3979662    2985323  -1.333    0.215
years           2412       1466   1.645    0.134

Residual standard error: 15380 on 9 degrees of freedom
Multiple R-squared:  0.2311,	Adjusted R-squared:  0.1457 
F-statistic: 2.706 on 1 and 9 DF,  p-value: 0.1344

[1] "slope as % of average"
      years 
0.002590975 
[1] "Ftar/Btrig"
[1] 2.7e-01 1.0e+06
