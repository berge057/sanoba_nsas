[1] "annual risk"

Call:
lm(formula = risk ~ years, data = lmFrame)

Residuals:
      Min        1Q    Median        3Q       Max 
-0.006546 -0.003636 -0.001455  0.003500  0.010636 

Coefficients:
              Estimate Std. Error t value Pr(>|t|)
(Intercept) -1.4720909  1.1139691  -1.321    0.219
years        0.0007273  0.0005471   1.329    0.216

Residual standard error: 0.005738 on 9 degrees of freedom
Multiple R-squared:  0.1641,	Adjusted R-squared:  0.07123 
F-statistic: 1.767 on 1 and 9 DF,  p-value: 0.2165

[1] "5% tail"

Call:
lm(formula = SSB5per ~ years, data = lmFrame)

Residuals:
   Min     1Q Median     3Q    Max 
-36657 -18933  -2984  14696  41862 

Coefficients:
            Estimate Std. Error t value Pr(>|t|)   
(Intercept) 23801099    5449550   4.368  0.00180 **
years         -11117       2677  -4.154  0.00247 **
---
Signif. codes:  0 �***� 0.001 �**� 0.01 �*� 0.05 �.� 0.1 � � 1

Residual standard error: 28070 on 9 degrees of freedom
Multiple R-squared:  0.6572,	Adjusted R-squared:  0.6191 
F-statistic: 17.25 on 1 and 9 DF,  p-value: 0.002471

[1] "slope as % of average"
       years 
-0.009532273 
[1] "Ftar/Btrig"
[1] 2.1e-01 1.3e+06
