[1] "annual risk"

Call:
lm(formula = risk ~ years, data = lmFrame)

Residuals:
       Min         1Q     Median         3Q        Max 
-0.0046909 -0.0021909 -0.0006182  0.0016955  0.0076909 

Coefficients:
              Estimate Std. Error t value Pr(>|t|)
(Intercept)  0.3416545  0.7302960   0.468    0.651
years       -0.0001545  0.0003587  -0.431    0.677

Residual standard error: 0.003762 on 9 degrees of freedom
Multiple R-squared:  0.02021,	Adjusted R-squared:  -0.08866 
F-statistic: 0.1856 on 1 and 9 DF,  p-value: 0.6767

[1] "5% tail"

Call:
lm(formula = SSB5per ~ years, data = lmFrame)

Residuals:
   Min     1Q Median     3Q    Max 
-24447 -14134   6826  11953  18934 

Coefficients:
            Estimate Std. Error t value Pr(>|t|)
(Intercept) -3061769    3163904  -0.968    0.358
years           1986       1554   1.278    0.233

Residual standard error: 16300 on 9 degrees of freedom
Multiple R-squared:  0.1536,	Adjusted R-squared:  0.05955 
F-statistic: 1.633 on 1 and 9 DF,  p-value: 0.2332

[1] "slope as % of average"
      years 
0.002023249 
[1] "Ftar/Btrig"
[1] 2.6e-01 1.2e+06
