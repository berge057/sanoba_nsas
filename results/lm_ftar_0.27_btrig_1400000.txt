[1] "annual risk"

Call:
lm(formula = risk ~ years, data = lmFrame)

Residuals:
      Min        1Q    Median        3Q       Max 
-0.004745 -0.003473  0.000800  0.001873  0.008582 

Coefficients:
              Estimate Std. Error t value Pr(>|t|)  
(Intercept)  1.8405273  0.8368085   2.199   0.0554 .
years       -0.0008909  0.0004110  -2.168   0.0583 .
---
Signif. codes:  0 �***� 0.001 �**� 0.01 �*� 0.05 �.� 0.1 � � 1

Residual standard error: 0.004311 on 9 degrees of freedom
Multiple R-squared:  0.343,	Adjusted R-squared:   0.27 
F-statistic: 4.699 on 1 and 9 DF,  p-value: 0.05834

[1] "5% tail"

Call:
lm(formula = SSB5per ~ years, data = lmFrame)

Residuals:
   Min     1Q Median     3Q    Max 
-20714 -15470 -10808   9367  39677 

Coefficients:
            Estimate Std. Error t value Pr(>|t|)  
(Intercept) -9184439    4256304  -2.158   0.0593 .
years           4994       2090   2.389   0.0406 *
---
Signif. codes:  0 �***� 0.001 �**� 0.01 �*� 0.05 �.� 0.1 � � 1

Residual standard error: 21930 on 9 degrees of freedom
Multiple R-squared:  0.388,	Adjusted R-squared:   0.32 
F-statistic: 5.706 on 1 and 9 DF,  p-value: 0.04064

[1] "slope as % of average"
      years 
0.005080502 
[1] "Ftar/Btrig"
[1] 2.7e-01 1.4e+06
