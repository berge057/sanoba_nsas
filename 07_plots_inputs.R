#-------------------------------------------------------------------------------
# WKNSMSE
#
# Author: Benoit Berges
#         WMR, The Netherland
# email: benoit.berges@wur.nl
#
#  MSE of North Sea Herring
#
# Date: 2018/11/18
#
# Build for R3.5.1, 64bits
#-------------------------------------------------------------------------------

#-------------------------------------------------------------------------------
# 1) load packages 
#    setup paths 
#    load functions
#-------------------------------------------------------------------------------

rm(list=ls())

library(minpack.lm)  # install.packages("minpack.lm")
library(stats)
require(reshape2)
require(ggplot2)
library(gridExtra)
library(RColorBrewer)
library(grid)
library(FLSAM)
library(ggplotFL)

assessment_name   <- "NSAS_sanoba"

# paths to different subfolders
dataPath      <- file.path(".","data/")
modelPath     <- file.path(".","model/")
resultsPath   <- file.path(".","results/")
scriptPath    <- file.path(".","side_scripts/")
functionPath  <- file.path(".","functions/")
figurePath    <- file.path(".","figures/")

################################################
# plotting
################################################

load(file.path(dataPath,'SST_NSAS.RData'))
load(file.path(dataPath,'SST_NSAS_hist.RData'))
load(file.path(dataPath,'SST_NSAS_rcp.RData'))



ggplot()+
  geom_line(data=month_subset,aes(x=year,y=sst))+
  geom_line(data=hist_sst,aes(x=year,y=sst),col='blue')+
  geom_line(data=rcp26_proj_sst,aes(x=year,y=sst),col='green')+
  geom_line(data=rcp45_proj_sst,aes(x=year,y=sst),col='red')+
  geom_line(data=rcp60_proj_sst,aes(x=year,y=sst),col='grey')+
  geom_line(data=rcp85_proj_sst,aes(x=year,y=sst),col='yellow')

ggplot()+
  geom_line(data=month_subset,aes(x=year,y=sst))+
  geom_line(data=hist_sst,aes(x=year,y=sst),col='blue')+
  geom_line(data=rcp26_proj_sst,aes(x=year,y=sst),col='green')+
  geom_line(data=rcp45_proj_sst,aes(x=year,y=sst),col='red')+
  geom_line(data=rcp60_proj_sst,aes(x=year,y=sst),col='grey')+
  geom_line(data=rcp85_proj_sst,aes(x=year,y=sst),col='yellow')

mean(month_subset$sst[month_subset$year >= 2006])-mean(rcp26_proj_sst$sst[rcp26_proj_sst$year <= 2020])

