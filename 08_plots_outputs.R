#-------------------------------------------------------------------------------
# 1) load packages 
#    setup paths 
#    load functions
#-------------------------------------------------------------------------------

rm(list=ls())

library(minpack.lm)  # install.packages("minpack.lm")
library(stats)
require(reshape2)
require(ggplot2)
library(gridExtra)
library(RColorBrewer)
library(grid)
library(FLSAM)
library(ggplotFL)
library(tidyverse)

assessment_name   <- "NSAS_sanoba"

        # paths to different subfolders
dataPath      <- file.path(".","data/")
modelPath     <- file.path(".","model/")
resultsPath   <- file.path(".","results/")
scriptPath    <- file.path(".","side_scripts/")
functionPath  <- file.path(".","functions/")
figurePath    <- file.path(".","figures/")

source(file.path(functionPath,'format_df.R'))

scenariosList <- read.csv(file.path(dataPath,'scenarios.csv'))

metricsPeriod <- ac(2020:2030)

################################################
# process data
################################################

flagFirst <- TRUE

for(idxScenar in 1:dim(scenariosList)[1]){
  OM        <- scenariosList$OM[idxScenar]
  case      <- scenariosList$case[idxScenar]
  refPointsName <- scenariosList$refPoints[idxScenar]
  endYearShock  <- scenariosList$endYearShock[idxScenar]
  
  fileName <- paste0('NSAS_sanoba_',OM,'_',case,'_',refPointsName,'_1000.RData')
  print(fileName)
  
  if(!is.na(endYearShock)){
    idxRef <- scenariosList$reference[idxScenar] == scenariosList$caseFull
    OM_ref            <- scenariosList$OM[idxRef]
    case_ref          <- scenariosList$case[idxRef]
    refPointsName_ref <- scenariosList$refPoints[idxRef]
    
    fileNameRef <- paste0('NSAS_sanoba_',OM_ref,'_',case_ref,'_',refPointsName_ref,'_1000.RData')
    
    load(file.path(modelPath,'final_runs',fileNameRef))
    biolRef <- biol
  }
  
  load(file.path(modelPath,'final_runs',fileName))
  
  outRes <- format_df(biol,
                      biolRef,
                      OM,
                      refPointsName,
                      case,
                      metricsPeriod,
                      referencePoints,
                      endYearShock)
  
  if(!is.na(endYearShock)){
    
    # resistance
    resist.temp    <- outRes$resist
    resist.temp    <- resist.temp %>% select(year,iter,amp,resp,bioR3,bioR1) %>% pivot_longer(!year & !iter,names_to = 'type',values_to = 'data')
    resist.temp$OM <- OM
    resist.temp$refPoint <- refPointsName
    resist.temp$shock    <- endYearShock-2020
    resist.temp$scenar   <- paste0(OM, ' - ',refPointsName,' - shocksN=',endYearShock-2020)
    
    # resistance relative to no shocks
    resistNoShock.temp    <- outRes$resistNoShocks
    resistNoShock.temp    <- resistNoShock.temp %>% select(year,iter,amp,resp) %>% pivot_longer(!year & !iter,names_to = 'type',values_to = 'data')
    resistNoShock.temp$OM <- OM
    resistNoShock.temp$refPoint <- refPointsName
    resistNoShock.temp$shock    <- endYearShock-2020
    resistNoShock.temp$scenar   <- paste0(OM, ' - ',refPointsName,' - shocksN=',endYearShock-2020)
    
    # resilience relative to management targets
    resil.temp     <- outRes$resil
    resil.temp     <- resil.temp %>% select(year,iter,recovRate2030,recovRate2041,recovSpeed) %>% pivot_longer(!year & !iter,names_to = 'type',values_to = 'data')
    resil.temp$OM  <- OM
    resil.temp$refPoint <- refPointsName
    resil.temp$shock    <- endYearShock-2020
    resil.temp$scenar   <- paste0(OM, ' - ',refPointsName,' - shocksN=',endYearShock-2020)
    
    # resilience relative to no shocks
    resilNoShock.temp     <- outRes$resilNoShocks
    resilNoShock.temp     <- resilNoShock.temp %>% select(year,iter,recovRate2030,recovRate2041,recovSpeed) %>% pivot_longer(!year & !iter,names_to = 'type',values_to = 'data')
    resilNoShock.temp$OM  <- OM
    resilNoShock.temp$refPoint <- refPointsName
    resilNoShock.temp$shock    <- endYearShock-2020
    resilNoShock.temp$scenar   <- paste0(OM, ' - ',refPointsName,' - shocksN=',endYearShock-2020)
  }
  
  # fbar
  fbar.temp    <- outRes$fbar.df
  fbar.temp    <- fbar.temp %>% select(year,iter,data) %>% pivot_longer(!year & !iter,names_to = 'type',values_to = 'data')
  fbar.temp$OM  <- OM
  fbar.temp$refPoint <- refPointsName
  fbar.temp$shock    <- endYearShock-2020
  fbar.temp$scenar   <- paste0(OM, ' - ',refPointsName,' - shocksN=',endYearShock-2020)
  
  
  catch.temp     <- outRes$catch.df
  catch.temp     <- catch.temp %>% select(year,iter,data) %>% pivot_longer(!year & !iter,names_to = 'type',values_to = 'data')
  catch.temp$OM  <- OM
  catch.temp$refPoint <- refPointsName
  catch.temp$shock    <- endYearShock-2020
  catch.temp$scenar   <- paste0(OM, ' - ',refPointsName,' - shocksN=',endYearShock-2020)
  
  ssb.temp     <- outRes$ssb.df
  ssb.temp     <- ssb.temp %>% select(year,iter,data) %>% pivot_longer(!year & !iter,names_to = 'type',values_to = 'data')
  ssb.temp$OM  <- OM
  ssb.temp$refPoint <- refPointsName
  ssb.temp$shock    <- endYearShock-2020
  ssb.temp$scenar   <- paste0(OM, ' - ',refPointsName,' - shocksN=',endYearShock-2020)
  
  rec.temp     <- outRes$rec.df
  rec.temp     <- rec.temp %>% select(year,iter,data) %>% pivot_longer(!year & !iter,names_to = 'type',values_to = 'data')
  rec.temp$OM  <- OM
  rec.temp$refPoint <- refPointsName
  rec.temp$shock    <- endYearShock-2020
  rec.temp$scenar   <- paste0(OM, ' - ',refPointsName,' - shocksN=',endYearShock-2020)
  
  if(flagFirst){
    resist.all  <- resist.temp
    resil.all   <- resil.temp
    resistNoShock.all  <- resistNoShock.temp
    resilNoShock.all   <- resilNoShock.temp
    fbar.all    <- fbar.temp
    catch.all   <- catch.temp
    ssb.all     <- ssb.temp
    rec.all     <- rec.temp
    
    flagFirst <- FALSE
  }else{
    if(!is.na(endYearShock)){
      resist.all  <- rbind(resist.all,resist.temp)
      resil.all   <- rbind(resil.all,resil.temp)
      resistNoShock.all  <- rbind(resistNoShock.all,resistNoShock.temp)
      resilNoShock.all   <- rbind(resilNoShock.all,resilNoShock.temp)
    }
    fbar.all    <- rbind(fbar.all,fbar.temp)
    catch.all   <- rbind(catch.all,catch.temp)
    ssb.all     <- rbind(ssb.all,ssb.temp)
    rec.all     <- rbind(rec.all,rec.temp)
  }
}

ssb.all_wide <- ssb.all %>% pivot_wider(names_from = iter, values_from = data)
ssb.all_wide$shock[is.na(ssb.all_wide$shock)] <- 0

fbar.all_wide <- fbar.all %>% pivot_wider(names_from = iter, values_from = data)
fbar.all_wide$shock[is.na(fbar.all_wide$shock)] <- 0

catch.all_wide <- catch.all %>% pivot_wider(names_from = iter, values_from = data)
catch.all_wide$shock[is.na(catch.all_wide$shock)] <- 0

rec.all_wide <- rec.all %>% pivot_wider(names_from = iter, values_from = data)
rec.all_wide$shock[is.na(rec.all_wide$shock)] <- 0


######################
## plotting
######################

scaling_factor <- 1.5
png(file.path(figurePath,paste0('rec_traj.png')),
    width = 12*scaling_factor, height = 8*scaling_factor, units = "cm", res = 300, pointsize = 10)

p <- ggplot(rec.all_wide,aes(x=year,y=`50%`,col=as.factor(shock)))+
      geom_line()+
      geom_ribbon(aes(ymin =`5%`,ymax=`95%`,fill=as.factor(shock)),alpha=0.5,colour = NA)+
      geom_vline(xintercept = 2020)+
      labs(y='rec')+
      facet_grid(OM~refPoint)

print(p)
dev.off()

scaling_factor <- 1.5
png(file.path(figurePath,paste0('SSB_traj.png')),
    width = 12*scaling_factor, height = 8*scaling_factor, units = "cm", res = 300, pointsize = 10)

p <- ggplot(ssb.all_wide,aes(x=year,y=`50%`,col=as.factor(shock)))+
      geom_line()+
      geom_ribbon(aes(ymin =`5%`,ymax=`95%`,fill=as.factor(shock)),alpha=0.5,colour = NA)+
      geom_vline(xintercept = 2020)+
      geom_hline(yintercept = referencePoints$Blim,linetype='dotted')+
      labs(y='SSB')+
      facet_grid(OM~refPoint)

print(p)
dev.off()

scaling_factor <- 1.5
png(file.path(figurePath,paste0('fbar_traj.png')),
    width = 12*scaling_factor, height = 8*scaling_factor, units = "cm", res = 300, pointsize = 10)

p <- ggplot(fbar.all_wide,aes(x=year,y=`50%`,col=as.factor(shock)))+
      geom_line()+
      geom_vline(xintercept = 2020)+
      geom_ribbon(aes(ymin =`5%`,ymax=`95%`,fill=as.factor(shock)),alpha=0.5,colour = NA)+
      geom_hline(yintercept = 0.3,linetype='dotted')+
      geom_hline(yintercept = 0.175,linetype='dotted')+
      labs(y='Fbar')+
      facet_grid(OM~refPoint)

print(p)
dev.off()

scaling_factor <- 1.5
png(file.path(figurePath,paste0('catch_traj.png')),
    width = 12*scaling_factor, height = 8*scaling_factor, units = "cm", res = 300, pointsize = 10)

p <- ggplot(catch.all_wide,aes(x=year,y=`50%`,col=as.factor(shock)))+
      geom_line()+
      geom_vline(xintercept = 2020)+
      geom_ribbon(aes(ymin =`5%`,ymax=`95%`,fill=as.factor(shock)),alpha=0.5,colour = NA)+
      labs(y='Catch')+
      facet_grid(OM~refPoint)

print(p)
dev.off()

scaling_factor <- 1.5
png(file.path(figurePath,paste0('resiliance_management.png')),
    width = 12*scaling_factor, height = 8*scaling_factor, units = "cm", res = 300, pointsize = 10)

p <- ggplot(resil.all,aes(x=scenar,y=data,col=refPoint))+
      geom_boxplot()+
      theme(axis.text.x = element_text(angle=90))+
      facet_wrap(.~type,scale='free_y')

print(p)
dev.off()


scaling_factor <- 1.5
png(file.path(figurePath,paste0('resiliance_noShocks.png')),
    width = 12*scaling_factor, height = 8*scaling_factor, units = "cm", res = 300, pointsize = 10)

p <- ggplot(resilNoShock.all,aes(x=scenar,y=data,col=refPoint))+
  geom_boxplot()+
  theme(axis.text.x = element_text(angle=90))+
  facet_wrap(.~type,scale='free_y')

print(p)
dev.off()

scaling_factor <- 1.5
png(file.path(figurePath,paste0('resistance.png')),
    width = 12*scaling_factor, height = 8*scaling_factor, units = "cm", res = 300, pointsize = 10)

p <- ggplot(resist.all,aes(x=scenar,y=data,col=refPoint))+
      geom_boxplot()+
      theme(axis.text.x = element_text(angle=90))+
      facet_wrap(.~type,scale='free_y',ncol=4)

print(p)
dev.off()

scaling_factor <- 1.5
png(file.path(figurePath,paste0('resistance_noShocks.png')),
    width = 12*scaling_factor, height = 8*scaling_factor, units = "cm", res = 300, pointsize = 10)

p <- ggplot(resistNoShock.all,aes(x=scenar,y=data,col=refPoint))+
      geom_boxplot()+
      theme(axis.text.x = element_text(angle=90))+
      facet_wrap(.~type,scale='free_y',ncol=2)

print(p)
dev.off()
