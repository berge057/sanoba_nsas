setwd('C:/git/sanoba_nsas/attic/VB')

rm(list=ls())

base<-(read.csv("baseW.csv"))
base<-base[-1470,]
base$stock  <- as.factor(base$stock)
names(base)[3]<-"w"
SST<-read.csv("SST.csv")
surf<-read.csv("area.csv")
#par(mfrow=c(3,4))

stocknonbalt<-c(2,4:6,8:14,16:18,20)
results<-data.frame()
for (i in stocknonbalt)

       {
###########
#calcule la courbe de croissance du stock
  print(i)
stock<-levels(base$stock)[i]
growth.fit<- nls(w~winf*(1-exp(-k*(age-t0)))^3,data=base[base$stock==stock,],start=list(winf=0.5,k=0.3,t0=-0.5))
winfest<-summary(growth.fit)$coefficients[1,1]
kest<-summary(growth.fit)$coefficients[2,1]
t0est<-summary(growth.fit)$coefficients[3,1]
YC<-as.double(levels(as.factor(base$YC[base$stock==stock])))
t<-seq(from=0,to=max(base$age[base$stock==stock]),by=0.1)
#windows()
plot(t,winfest*(1-exp(-kest*(t-t0est)))^3,type="l",ylim=c(0,max(base$w[base$stock==stock])*1.1),main= paste(stock," nls"),xlab="age",ylab="weight")
points(base$age[base$stock==stock],base$w[base$stock==stock])
#############


#########################""""
#cree les tableau pour stocker les résultats
paraestnls<-data.frame(stock,YC,rep(NA,length(YC)),rep(NA,length(YC)),rep(NA,length(YC)),rep(NA,length(YC)),rep(NA,length(YC)),rep(NA,length(YC)),rep(NA,length(YC)))
SS<-rep(0,length(YC))
names(paraestnls)<-c("stock","YC","winf","k","t0","SST","dens","nb_years_W","nb_years_SST")




#################################
# ajuste le model pour chaque cohorte
for (j in 1:length(YC))
{ 
years<-base$year[base$stock==stock & base$YC==YC[j] ]
paraestnls$nb_years_W[j]<-length(years)
paraestnls$nb_years_SST[j]<-length(SST$Temp[SST$stock==stock & is.element(SST$year,years)])
paraestnls$SST[j]<- mean(SST$Temp[SST$stock==stock & is.element(SST$year,years)])
try(paraestnls$dens[j]<-mean (na.omit(base$SSB[base$stock==stock & is.element(base$year,years)]))/ surf$area[surf$stock==stock],silent=T)
paraestnls$nb_years_SST[j]<-length(base$SSB[base$YC==YC[j]  & base$stock==stock & is.element(base$year,years)])

if (length(years)>(max( base$age[base$stock==stock])-min( base$age[base$stock==stock])-1))
{
 try(
{
growth.fitI<- nls(w~winf*(1-exp(-k*(age-t0)))^3,data=base[base$stock==stock & base$YC==YC[j] ,],start=list(winf=0.4,k=0.3,t0=-0.5))

#growth.fitI<- nls(w~winf*(1-exp(-k*(age+exp(lnt0))))^3,data=base[base$stock==stock & base$YC==YC[j] ,])

plot(t,winfestI*(1-exp(-kestI*(t-t0estI)))^3,type="l",ylim=c(0,max(base$w[base$stock==stock])*1.1),
     main= paste(stock," nls"),xlab="age",ylab="weight")
points(base$age[base$stock==stock & base$YC==YC[j]],base$w[base$stock==stock & base$YC==YC[j]])


SS[j]<-sum((residuals(growth.fitI))^2)
winfestI<-summary(growth.fitI)$coefficients[1,1]
kestI<-summary(growth.fitI)$coefficients[2,1]
t0estI<-(summary(growth.fitI)$coefficients[3,1])
lines(t,winfestI*(1-exp(-kestI*(t-t0estI)))^3)
paraestnls$winf[j]<-winfestI
paraestnls$k[j]<-kestI
paraestnls$t0[j]<-t0estI
rm(winfestI,kestI,t0estI)
}  )
}
}
SSnls <-sum(SS)

results<-rbind(results,paraestnls)

resultssave<-results
}




###############################################################################
# ploter les resultats
results<-resultssave
par(mfrow=c(3,5))
for (i in c(1:15))
{
stock<-levels(as.factor(results$stock))[i]
plot(winf~SST, data=results[results$stock==stock,],main=stock[1],ylab="winf",xlab="SST")
SSTT<-c(min(na.omit(results$SST[results$stock==stock])),max(na.omit((results$SST[results$stock==stock]))))
lines(SSTT,lm(winf~SST, data=results[results$stock==stock,])$coefficients[1]+SSTT*lm(winf~SST, data=results[results$stock==stock,])$coefficients[2])
}

windows()
par(mfrow=c(3,5))
for (i in c(1:15))
{
stock<-levels(as.factor(results$stock))[i]
plot(k~SST, data=results[results$stock==stock,],main=stock[1],ylab="k",xlab="SST")
SSTT<-c(min(na.omit(results$SST[results$stock==stock])),max(na.omit((results$SST[results$stock==stock]))))
lines(SSTT,lm(k~SST, data=results[results$stock==stock,])$coefficients[1]+SSTT*lm(k~SST, data=results[results$stock==stock,])$coefficients[2])
}

windows()
par(mfrow=c(3,5))
for (i in c(1:15))
{
stock<-levels(as.factor(results$stock))[i]
try(plot(winf~dens, data=results[results$stock==stock,],main=stock[1],ylab="winf",xlab="density"),silent=T)
try(SSBT<-c(min(na.omit(results$dens[results$stock==stock])),max(na.omit((results$dens[results$stock==stock])))),silent=T)
try(lines(SSBT,lm(winf~dens, data=results[results$stock==stock,])$coefficients[1]+SSBT*lm(winf~dens, data=results[results$stock==stock,])$coefficients[2]),silent=T )
}

windows()
par(mfrow=c(3,5))
for (i in c(1:15))
{
stock<-levels(as.factor(results$stock))[i]
try(plot(k~dens, data=results[results$stock==stock,],main=stock[1],ylab="k",xlab="density"),silent=T)
try(SSBT<-c(min(na.omit(results$dens[results$stock==stock])),max(na.omit((results$dens[results$stock==stock])))),silent=T)
try(lines(SSBT,lm(k~dens, data=results[results$stock==stock,])$coefficients[1]+SSBT*lm(k~dens, data=results[results$stock==stock,])$coefficients[2]),silent=T)
}



##############################################################################
#essais glm
results<-na.omit(resultssave)
SSTres<-rep(0,dim(results)[1])
densres<-rep(0,dim(results)[1])
SSTmean<-rep(0,dim(results)[1])
densmean<-rep(0,dim(results)[1])
results<-data.frame(results,SSTres,SSTmean,densres,densmean)
for (i in c(1:15))
{
stock<-levels(as.factor(results$stock))[i]
results$SSTmean[results$stock==stock]<-mean(results$SST[results$stock==stock])
results$SSTres[results$stock==stock]<-results$SST[results$stock==stock]-mean(results$SST[results$stock==stock])                                                                              
try(results$densmean[results$stock==stock]<-mean(results$dens[results$stock==stock]),silent=T)
try(results$densres[results$stock==stock]<-results$dens[results$stock==stock]-mean(results$dens[results$stock==stock]),silent=T)
}

#model selection Winf
ee1<-glm(winf~SSTmean,data=results,family=Gamma); ee1$aic                   #AIC -692  #this one wins   
ee2<-glm(winf~SSTres,data=results,family=Gamma)   ; ee2$aic                   #AIC -414
ee3<-glm(winf~densmean,data=results,family=Gamma)  ; ee3$aic                   #AIC -526
ee4<-glm(winf~densres,data=results,family=Gamma) ; ee4$aic                     #AIC -414

ee5<-glm(winf~SSTmean+SSTres,data=results,family=Gamma) ; ee5$aic             #AIC -698  #this one wins 
ee6<-glm(winf~SSTmean+densmean,data=results,family=Gamma) ; ee6$aic            #AIC -695   
ee7<-glm(winf~SSTmean+densres,data=results,family=Gamma); ee7$aic              #AIC -693  

ee8<-glm(winf~SSTmean+SSTres+densmean,data=results,family=Gamma) ; ee8$aic     #AIC -702
ee9<-glm(winf~SSTmean+SSTres+densres,data=results,family=Gamma) ; ee9$aic      #AIC -701
ee10<-glm(winf~SSTmean+densres+densmean,data=results,family=Gamma) ; ee10$aic     #AIC -696

ee11<-glm(winf~SSTmean+SSTres+densmean+densres,data=results,family=Gamma); ee11$aic      #AIC -704
ee12<-glm(winf~SSTmean+SSTres+densmean+densres,data=results); ee12$aic      #AIC -704
ee13<-glm(log(winf)~SSTmean+SSTres+densmean+densres,data=results); ee13$aic      #AIC -704


#model selection k
e1<-glm(k~SSTmean,data=results,family=Gamma)   ; e1$aic                     #AIC -254 #this one wins   
e2<-glm(k~SSTres,data=results,family=Gamma)    ; e2$aic                    #AIC -176
e3<-glm(k~densmean,data=results,family=Gamma)  ; e3$aic                     #AIC -218
e4<-glm(k~densres,data=results,family=Gamma)   ; e4$aic                     #AIC -176

e5<-glm(k~SSTmean+SSTres,data=results,family=Gamma) ; e5$aic               #AIC -255#this one wins 
e6<-glm(k~SSTmean+densmean,data=results,family=Gamma) ; e6$aic              #AIC -252   
e7<-glm(k~SSTmean+densres,data=results,family=Gamma) ; e7$aic               #AIC -256.1  

e8<-glm(k~SSTmean+densres+densmean,data=results,family=Gamma) ; e8$aic      #AIC -254 #this one wins 
e9<-glm(k~SSTmean+SSTres+densres,data=results,family=Gamma) ; e9$aic       #AIC -256.2  #this one wins 

e10<-glm(k~SSTmean+SSTres+densmean+densres,data=results) ; e10$aic       #AIC -256  #this one wins 


 
##############################################################################################################################################################
##############################################################################################################################################################
#regarde les correlations entre les parametres et les variables explicatives

# function to calculated cor test with autocor according to modified chelton method
source('M:/ref biblio/méthodes analyse/cor test with autocor.r')



 results<-resultssave
regres<-data.frame(stock=levels(as.factor(results$stock)),r.winf_SST=rep(NA,15),r.winf_SSTinf=rep(NA,15),r.winf_SSTsup=rep(NA,15),p.val.winf_SST=rep(NA,15),star1=rep(NA,15),df1=rep(NA,15),r.winf_dens=rep(NA,15),r.winf_densinf=rep(NA,15),r.winf_denssup=rep(NA,15),p.val.winf_dens=rep(NA,15),star2=rep(NA,15),df2=rep(NA,15),r.k_SST=rep(NA,15),r.k_SSTinf=rep(NA,15),r.k_SSTsup=rep(NA,15),p.val.k_SST=rep(NA,15),star3=rep(NA,15),df3=rep(NA,15),r.k_dens=rep(NA,15),r.k_densinf=rep(NA,15),r.k_denssup=rep(NA,15),p.val.k_dens=rep(NA,15),star4=rep(NA,15),df4=rep(NA,15))
for (i in 1:15)
{
stock<-levels(as.factor(results$stock) )[i]
regres$r.winf_SST[i]<-cor.test.auto(results$winf[results$stock==stock],results$SST[results$stock==stock])$estimate
regres$r.winf_SSTinf[i]<-cor.test.auto(results$winf[results$stock==stock],results$SST[results$stock==stock])$conf.int[1]
regres$r.winf_SSTsup[i]<-cor.test.auto(results$winf[results$stock==stock],results$SST[results$stock==stock])$conf.int[2]
regres$df1[i]<-cor.test.auto(results$winf[results$stock==stock],results$SST[results$stock==stock])$parameter
pev<-cor.test.auto(results$winf[results$stock==stock],results$SST[results$stock==stock])$p.value
regres$p.val.winf_SST[i]<-pev
if (pev>0.05) {pev<-""}  else { if (pev>0.01) {pev<-"*"} else {pev<-"**"}}
regres$star1[i]<-pev
rm(pev)
try(regres$r.winf_dens[i]<-cor.test.auto(results$winf[results$stock==stock],results$dens[results$stock==stock])$estimate,silent=T)
try(regres$r.winf_densinf[i]<-cor.test.auto(results$winf[results$stock==stock],results$dens[results$stock==stock])$conf.int[1],silent=T)
try(regres$r.winf_denssup[i]<-cor.test.auto(results$winf[results$stock==stock],results$dens[results$stock==stock])$conf.int[2],silent=T)                                                                                                                             
try(regres$df2[i]<-cor.test.auto(results$winf[results$stock==stock],results$dens[results$stock==stock])$parameter,silent=T)
try (pev<-cor.test.auto(results$winf[results$stock==stock],results$dens[results$stock==stock])$p.value,silent=T)
try(regres$p.val.winf_dens[i]<-pev,silent=T)
try(if (pev>0.05) {pev<-""}  else { if (pev>0.01) {pev<-"*"} else {pev<-"**"}},silent=T)
try(regres$star2[i]<-pev,silent=T)
try(rm(pev),silent=T)
regres$r.k_SST[i]<-cor.test.auto(results$k[results$stock==stock],results$SST[results$stock==stock])$estimate
regres$r.k_SSTinf[i]<-cor.test.auto(results$k[results$stock==stock],results$SST[results$stock==stock])$conf.int[1]
regres$r.k_SSTsup[i]<-cor.test.auto(results$k[results$stock==stock],results$SST[results$stock==stock])$conf.int[2]                                                                                                       
regres$df3[i]<-cor.test.auto(results$k[results$stock==stock],results$SST[results$stock==stock])$parameter
pev<-cor.test.auto(results$k[results$stock==stock],results$SST[results$stock==stock])$p.value
regres$p.val.k_SST[i]<-pev
if (pev>0.05) {pev<-""}  else { if (pev>0.01) {pev<-"*"} else {pev<-"**"}}
regres$star3[i]<-pev
rm(pev)
try(regres$r.k_dens[i]<-cor.test.auto(results$k[results$stock==stock],results$dens[results$stock==stock])$estimate,silent=T)
try(regres$r.k_densinf[i]<-cor.test.auto(results$k[results$stock==stock],results$dens[results$stock==stock])$conf.int[1],silent=T)
try(regres$r.k_denssup[i]<-cor.test.auto(results$k[results$stock==stock],results$dens[results$stock==stock])$conf.int[2],silent=T)                                                                                                                       
try(regres$df4[i]<-cor.test.auto(results$k[results$stock==stock],results$dens[results$stock==stock])$parameter,silent=T)
try (pev<-cor.test.auto(results$k[results$stock==stock],results$dens[results$stock==stock])$p.value,silent=T)
try(regres$p.val.k_dens[i]<-pev,silent=T)
try(if (pev>0.05) {pev<-""}  else { if (pev>0.01) {pev<-"*"} else {pev<-"**"}},silent=T)
try(regres$star4[i]<-pev,silent=T)
try(rm(pev),silent=T)
}


regsave<-regres
################################################################################
################################################################################
#meta analitycal test  1
Ftest<-data.frame(correl=rep(0,4),chisq=rep(0,4), p.val=rep(0,4))
Ztest<-Ftest
names(Ztest)[2]<-"Z"
Zwtest<-Ztest
cols<-c(5,11,17,23)

for (i in 1:4)
{
a<-cols[i]
Zw<-sum(na.omit(regres[,a+2])*qnorm(na.omit(regres[,a]))) / sqrt(sum(na.omit(regres[,a+2]^2)))
Zwtest$correl[i]<-names(regres)[a]
Zwtest$Z[i]<-Zw
Zwtest$p.val[i]<-1-pnorm(abs(Zw))
}

Zwtest
  